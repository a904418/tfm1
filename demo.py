#ELBUENO
from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *

import os
import sys
import time
import datetime
import argparse
import cv2
import json
import numpy as np
from PIL import Image

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator

global lista, listaframes, listaclass, listalocation, listascore
lista=[]
listaframes=[]
listaclass=[]
listalocation=[]
listascore=[]
#def detection():
	#np.savetxt("listaframes.txt", listaframes, fmt="%d")
 	#np.savetxt("listaclass.txt", listaclass, fmt="%s", delimiter=',')
	#np.savetxt("listalocation.txt", listalocation, fmt="%f")
	#np.savetxt("listascore.txt", listascore, fmt="%f")

class NumpyEncoder(json.JSONEncoder):
			def default(self, obj):
				if isinstance(obj, np.integer):
					return int(obj)
				elif isinstance(obj, np.floating):
					return float(obj)
				elif isinstance(obj, np.ndarray):
					return obj.tolist()
				return json.JSONEncoder.default(self, obj)


class Object:
	def __init__(self, num_frame, object_class, location, score):
		self.num_frame= num_frame
		self.object_class = object_class
		self.location = location
		self.score = score
		
def detection():

	with open('lista.txt', 'w') as file:
		for obj in lista:
			file.write("frame: ")
			file.write("%d\r\n" % obj.num_frame)	
			file.write("object: ")
			file.write("%s\r\n" % obj.object_class)
			file.write("location: ")
			local=np.array(obj.location)
			dumped= json.dumps(local, cls=NumpyEncoder)
			file.write(json.dumps(dumped))
			file.write("\nscore: ")
			file.write("%f\r\n" % obj.score)

	for obj in lista:
		print("frame:", obj.num_frame,"object:", obj.object_class,"location:", obj.location, "score:", obj.score)	

def changeBGR2RGB(img):
    b = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    r = img[:, :, 2].copy()

    # RGB > BGR
    img[:, :, 0] = r
    img[:, :, 1] = g
    img[:, :, 2] = b

    return img


def changeRGB2BGR(img):
    r = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    b = img[:, :, 2].copy()

    # RGB > BGR
    img[:, :, 0] = b
    img[:, :, 1] = g
    img[:, :, 2] = r

    return img

num_frame=0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--image_folder", type=str, default="data/samples", help="path to dataset")
    parser.add_argument("video", help='input video')
    parser.add_argument("--model_def", type=str, default="config/yolov3.cfg", help="path to model definition file")
    parser.add_argument("--weights_path", type=str, default="weights/yolov3.weights", help="path to weights file")
    parser.add_argument("--class_path", type=str, default="data/coco.names", help="path to class label file")
    parser.add_argument("--conf_thres", type=float, default=0.3, help="object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.3, help="iou thresshold for non-maximum suppression")
    parser.add_argument("--batch_size", type=int, default=1, help="size of the batches")
    parser.add_argument("--n_cpu", type=int, default=0, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=608, help="size of each image dimension")
    parser.add_argument("--checkpoint_model", type=str, help="path to checkpoint model")
    opt = parser.parse_args()
    print(opt)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Darknet(opt.model_def, img_size=opt.img_size).to(device)
    if opt.weights_path.endswith(".weights"):
        # Load darknet weights
        model.load_darknet_weights(opt.weights_path)
    else:
        # Load checkpoint weights
        model.load_state_dict(torch.load(opt.weights_path))
    model.eval()  # Set in evaluation mode
    classes = load_classes(opt.class_path)
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor
    #if opt.vedio_file.endswith(".mp4"):
    cap = cv2.VideoCapture(opt.video)
    colors = np.random.randint(0, 255, size=(len(classes), 3), dtype="uint8")
    a=[]
    time_begin = time.time()
    NUM = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    #NUM=0
 
    while cap.isOpened():
        ret, img = cap.read()
        if ret is False:
            break
        img = cv2.resize(img, (opt.img_size,img.shape[0]*opt.img_size//img.shape[1]), interpolation=cv2.INTER_CUBIC)

        #PILimg = np.array(Image.fromarray(cv2.cvtColor(img,cv2.COLOR_BGR2RGB)))
        #imgTensor = transforms.ToTensor()(PILimg)
        #基于pytorch的yolov3 从github拉的
        # yolov3如何改进成可以对视频进行实时分析
        #以下的代码可以在utils的文件里找到 是在data loader里面对数据进行处理的，那么也可以把代码直接复制过来用
        #需要注意的是 PIL读取的图片是RGB的 这里的图片是BGR的 是opencv读取的
        #进行转换
        #转换使用自己写的函数
        #前面的都很简单 都是从detect的代码复制过来的，加了一个打开视频cap
        #然后有很多人的疑问就是代码直接拉过来不知道怎么改，不知道图片怎么改成张量
        #但是这个img转化之后缺少一个维度
        RGBimg=changeBGR2RGB(img)
        imgTensor = transforms.ToTensor()(RGBimg)
        imgTensor, _ = pad_to_square(imgTensor, 0)
        imgTensor = resize(imgTensor, 608)
        #需要用这个unsqueeze去转化
        #是看了莫烦的机器学习想到的 结合报错信息
        imgTensor = imgTensor.unsqueeze(0)
        imgTensor = Variable(imgTensor.type(Tensor))
        #下面再预测就可以了
        #展示一下吧
        with torch.no_grad():
            detections = model(imgTensor)
            detections = non_max_suppression(detections, opt.conf_thres, opt.nms_thres)
        num_frame=num_frame+1
        a.clear()
        if detections is not None:
            a.extend(detections)
        b=len(a)
        if len(a)  :
            for detections in a:
                if detections is not None:
                    detections = rescale_boxes(detections, opt.img_size, RGBimg.shape[:2])
                    unique_labels = detections[:, -1].cpu().unique()
                    n_cls_preds = len(unique_labels)
                    for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                        box_w = x2 - x1
                        box_h = y2 - y1
                        color = [int(c) for c in colors[int(cls_pred)]]
                        #print(cls_conf)
                        img = cv2.rectangle(img, (x1, y1 + box_h), (x2, y1), color, 2)
                        cv2.putText(img, classes[int(cls_pred)], (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
                        cv2.putText(img, str("%.2f" % float(conf)), (x2, y2 - box_h), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                                    color, 2)
                        tarray=[x1, y1, x2, y2]
                        #lista.append( Object(num_frame, classes[int(cls_pred)], tarray, conf))
                        #print(num_frame)
                        #print(classes[int(cls_pred)])
                        listaframes.append(num_frame)
                        listaclass.append(classes[int(cls_pred)])
                        listalocation.append(tarray)
                        listascore.append(conf)
                        lista.append( Object(num_frame, classes[int(cls_pred)], tarray, conf))

            #print()
            #print()
        #cv2.putText(img,"Hello World!",(400,50),cv2.FONT_HERSHEY_PLAIN,2.0,(0,0,255),2)

        cv2.imshow('frame', changeRGB2BGR(RGBimg))
        #cv2.waitKey(0)

        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    time_end = time.time()
    time_total = time_end - time_begin
    print(NUM // time_total)
    detection()
    cap.release()
    cv2.destroyAllWindows()
