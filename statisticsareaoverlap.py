import sys
import json
import cv2
from pylab import *
import matplotlib.pyplot as plt
import numpy as np
from numpy import loadtxt
import argparse
import time
import collections
import colorsys
import os
import re
import ast


def adapt_location(location):
	if len(location) == 4:
		return location
	
	return [min(location[::2]), min(location[1::2]), max(location[::2]), max(location[1::2])]


class Object:
	def __init__(self, num_frame, object_class, location, score):			
		self.num_frame= num_frame
		self.object_class = object_class
		self.location = adapt_location(location)
		self.score = score
	
	def __repr__(self):
		return f"{self.object_class} with score {self.score} at frame {self.num_frame} and position {self.location}"

colors = {}

def load_object_file(filename):
	with open(filename, 'r') as f:
		frame = None
		score = None
		location = None
		name = None
		objects = []
		for line in f:			
			frame_match = re.match("frame: (\d+)", line)
			object_match = re.match("object: (\w+)", line)
			location_match = re.match("location: \"(.+)\"", line)
			score_match = re.match("score: (.+)", line)
			if frame_match:
				frame = int(frame_match[1]) 
			if object_match:
				name = object_match[1]
			if location_match:
				location = ast.literal_eval(location_match[1])
			if score_match:
				score = float(score_match[1])
				
				objects.append(Object(frame, name, location, score))
		return objects
			

def get_objects(files):
	return sorted([item for filename in files for item in load_object_file(filename)], key=lambda o: o.num_frame)
	
def  OverlapArea(rec1, rec2):
	area1=(rec1[2]-rec1[0])*(rec1[3]-rec1[1])
	area2=(rec2[2]-rec2[0])*(rec2[3]-rec2[1])
	dx=min(rec1[2], rec2[2]) - max(rec1[0], rec2[0])
	dy=min(rec1[3], rec2[3]) - max(rec1[1], rec2[1])
	if (dx >= 0) and (dy >= 0):
		areaover=dx*dy
		percent=areaover/(area1+area2-areaover)
		return percent
	else:
		return 0.0

	

	

def main():


	global listaframes, listaclass, listalocation, listascore, listastadistics
	listaframes=[]
	listaclass=[]
	listalocation=[]
	listascore=[]
	listastadistics=[]
	listarandom=[]
	num_frame=[]

	parser = argparse.ArgumentParser()
	
	parser.add_argument('objects', type=str, nargs='+', help='One or more files with a list of objects detections')

	args = parser.parse_args()
	
	objects = get_objects(args.objects)

	#listaframes=loadtxt("listaframes.txt", comments="#", delimiter=None, unpack=False, dtype=int)
	#listaclass=loadtxt("listaclass.txt", comments="#", delimiter=',', unpack=False, dtype=str)
	#listalocation=loadtxt("listalocation.txt", comments="#", delimiter=None, unpack=False, dtype=float)
	#listascore=loadtxt("listascore.txt", comments="#", delimiter=None, unpack=False)

	
	#contador=collections.Counter(listaclass)
	#print(contador)
	num_frames=1
	allframes=False
	counter=0

	dictgeneral={}
	dictobjectcounter={}
	dictrandom={}
	dictobjectxframe={}
	objeto=str("car")
	ubicacion=[0, 1, 3, 4]
	puntuacion=float(0.94)

	#while allframes is False:

	#	num_frames+=1
	
	length=len(objects)
	

	#rectangle1=listalocation[1]
	#l1=[rectangle1[0], rectangle1[1]]
	#r1=[rectangle1[2], rectangle1[3]]
	#print(l1,r1)
	
	#rectangle2=listalocation[1]
	#l2=[rectangle2[0], rectangle2[1]]
	#r2=[rectangle2[2], rectangle2[3]]
	#print(l2,r2)
	#if (l1[0] > r2[0] or l2[0] > r1[0]):
	#	Overlap=False
	#elif (l1[1] < r2[1] or l2[1] < l1[1]):
	#	Overlap=False
	#else:
	#	Overlap=True
	#print(Overlap)
	
	#for x in dictgeneral.values():
	#	print(x)
	#print(dictgeneral[1][1])
	#print(dictgeneral.keys())

	
	#i=0
	#listaframes=listaframes.tolist()
	#indice=listaframes.count(4)	
	#listaframes[-1]=0	
	objects[-1].num_frame=0	
	for x in range(0, length):
		key=x
		dictrandom.setdefault(key, [])
		dictrandom[x].append(objects[x].num_frame)
		dictrandom[x].append(objects[x].object_class)	
		if (x==len(objects)-1):
			print("Asigno ultimo valor")
			key=x+1
			dictrandom.setdefault(key, [])
			dictrandom[x+1].append("-")
			dictrandom[x+1].append("-")
	cont1=0	
	#print(dictrandom)

	while (num_frames<length) :
		for i in range(0,length):
			if (num_frames==dictrandom[i][0]):
				#print("Iteracion", i)
				unoigual=True
				entra=False
				if dictrandom[i][1] in dictobjectcounter:
					unoigual=False
					
					cont=0
					Area=0.0
					for x in range(0,i-1):
						
						if ((num_frames-1)==dictrandom[x][0]):
							if (dictrandom[i][1]==dictrandom[x][1]):
								
								
							
								#print(dictrandom[i][1])
								#print(listalocation[i])
								#print(dictrandom[x][1])
								#print(listalocation[x])
								#print("entra:", entra)
								Overlap=OverlapArea(objects[i].location, objects[x].location)
								#print(Overlap)
								
								
							
								if (Area>=0.9):
									#print(Area)
									unoigual=True
									break
								if (dictrandom[i][1] != dictrandom[x+1][1]) or (dictrandom[i][0] == dictrandom[x+1][0]):
									entra=True
									#print("EL SIGUIENTE ES UN OBJETO DISTINTO")						
									#print(unoigual)										
									if (unoigual==False and entra==True ):	
										#print("AÑADE****************************************************************************************************")
										#print("nºframe:", num_frames, "clase:", dictrandom[i][1], "cantidad:", dictobjectcounter[dictrandom[i][1]])
										#print(num_frames)
										#print(dictrandom[i][1])
										dictobjectcounter[dictrandom[i][1]]+=1			
				#if (num_frames != dictrandom[i+1][0]):
					
				if dictrandom[i][1] not in dictobjectcounter:
					#print("NO ESTABA XXXXXXXXXXXXXXXX")
					#print("nºframe:", num_frames)
					#print(dictrandom[i][1])
					
					dictobjectcounter[dictrandom[i][1]]=1
					#print(dictobjectcounter)
		

		#print("CAMBIO DE FRAME....................................")
		num_frames+=1
	
	#print(num_frames)	
	print(dictobjectcounter)

	num_frames=0 #Reinicio la variable num_frames para que entre en el siguiente bucle
	#Si tienes interés en saber la cantidad de objetos de una clase determinada tienes por frame:
	for key in dictobjectcounter:
		print(key)
		object=key
		cont1=0
		num_frames=0
		print(object)
		while (num_frames<length) :		
			for i in range(0, length):
				if dictrandom[i][0]==num_frames :
					if dictrandom[i][1]==object :
						cont1+=1
						#print(cont1)
					if (dictrandom[i+1][0] == num_frames+1)  :
						#print("Cambio de frame")
						dictobjectxframe[num_frames]=cont1
						cont1=0
					elif (dictrandom[i+1][0]==num_frames) :
						continue
					elif (dictrandom[i+1][0] != num_frames+1):
						cont1=0				
							
						
				
			num_frames+=1	
			
		print(dictobjectxframe)

		print("Media de", object, "por frame:", sum(list(dictobjectxframe.values()))/len(dictobjectxframe)) 
		
			#print( sum(dictobjectxframe.values()) /len(dictobjectxframe))
			#print(media)
				
		#plt.plot(dictobjectxframe.values())
		#xlabel('nº frame')
		#ylabel(object)
		#plt.show()
		#plt.savefig(object+'.png')

		#plt.clf()		
		dictobjectxframe.clear()	
						


if __name__ == '__main__': main()
